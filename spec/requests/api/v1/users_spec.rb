require 'rails_helper'

RSpec.describe "UsersController", type: :request do
  let(:user) { FactoryBot.create :user }
  let(:params) do
    {
      user: {
        email: user.email,
        password: user.password
      }
    }
  end

  describe "GET /" do
    context "when authenticated" do
      before do
        post new_user_session_url, params: params
        @token = response.headers['Authorization']
      end

      it "return status 200" do
        get api_v1_users_url, headers: { 'Authorization' => @token }

        expect(response.status).to eq 200
      end

      it "return users array" do
        get api_v1_users_url, headers: { 'Authorization' => @token }
        body_hash = JSON.parse(response.body)
        expect(body_hash.class.name).to eq "Array"
      end
    end

    context "when unauthenticated" do
      it "return 401" do
        get api_v1_users_url

        expect(response.status).to eq 401
      end
    end
  end

  describe "GET /:id" do
    context "when authenticated" do
      before do
        post new_user_session_url, params: params
        @token = response.headers['Authorization']
      end

      it "return status 200" do
        get api_v1_user_url(id: user.id), headers: { 'Authorization' => @token }
        expect(response.status).to eq 200
      end

      it "return user" do
        get api_v1_user_url(id: user.id), headers: { 'Authorization' => @token }
        body_hash = JSON.parse(response.body)
        expect(body_hash["id"]).to eq user.id
      end

      it "return 404 if not exist" do
        get api_v1_user_url(id: "a"), headers: { 'Authorization' => @token }
        expect(response.status).to eq 404
      end
    end

    context "when unauthenticated" do
      it "return 401" do
        get api_v1_user_url(id: user.id)

        expect(response.status).to eq 401
      end
    end
  end

  describe "PUT /:id" do
    context "when authenticated" do
      before do
        post new_user_session_url, params: params
        @token = response.headers['Authorization']
      end

      it "return status 200" do
        put api_v1_user_url(id: user.id), params: { user: { name: "Test" } }, headers: { 'Authorization' => @token }
        expect(response.status).to eq 200
      end

      it "return updated user" do
        put api_v1_user_url(id: user.id), params: { user: { name: "Test" } }, headers: { 'Authorization' => @token }
        body_hash = JSON.parse(response.body)
        expect(body_hash["name"]).to eq "Test"
      end

      it "return 404 if not exist" do
        put api_v1_user_url(id: "a"), params: { user: { name: "Test" } }, headers: { 'Authorization' => @token }
        expect(response.status).to eq 404
      end
    end

    context "when unauthenticated" do
      it "return 401" do
        put api_v1_user_url(id: user.id), params: { user: { name: "Test" } }

        expect(response.status).to eq 401
      end
    end
  end

  describe "DELETE /:id" do
    context "when authenticated" do
      before do
        post new_user_session_url, params: params
        @token = response.headers['Authorization']
        @user2 = FactoryBot.create :user
      end

      it "return status 200" do
        delete api_v1_user_url(id: @user2.id), headers: { 'Authorization' => @token }
        expect(response.status).to eq 200
      end

      it "delete user" do
        delete api_v1_user_url(id: @user2.id), headers: { 'Authorization' => @token }
        expect(User.find_by(id: @user2.id)).to eq nil
      end

      it "return 404 if not exist" do
        delete api_v1_user_url(id: "a"), headers: { 'Authorization' => @token }
        expect(response.status).to eq 404
      end
    end

    context "when unauthenticated" do
      it "return 401" do
        @user2 = FactoryBot.create :user
        delete api_v1_user_url(id: @user2.id)

        expect(response.status).to eq 401
      end
    end
  end
end