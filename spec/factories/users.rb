FactoryBot.define do
  factory :user do
    name { "Max" }
    password { "asdf1234" }
    last_name { "Power" }

    sequence(:email) { |n| "max#{n}@power.com1234" }
  end
end
