require 'rails_helper'

RSpec.describe User, type: :model do

  context "values are valid" do

    before do
      # build for creating a none saved instance, create for a saved instance
      @user = FactoryBot.build :user, name: "Max"
    end

    it "save the user with name and last_name" do
      @user.last_name = "Power"
      @user.save
      expect(@user.id).not_to eq(nil)
    end

    it "save the user with name without last_name" do
      @user.save
      expect(@user.id).not_to eq(nil)
    end
  end

  context "values are invalid" do

    before do
      @user = FactoryBot.build :user
    end

    it "do not save the user without email" do
      @user.email = nil
      @user.save
      expect(@user.id).to eq(nil)
    end

    it "do not save the user without name" do
      @user.name = nil
      @user.save
      expect(@user.id).to eq(nil)
    end

    it "do not save the user without password" do
      @user.password = nil
      @user.save
      expect(@user.id).to eq(nil)
    end
  end
end
