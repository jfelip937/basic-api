module Api
  module V1
    class UsersController < BaseController
      before_action :authenticate_user!

      def index
        render json: User.all.to_json
      end

      def show
        user = User.find(params[:id])
        render json: user
      end

      def update
        user = User.find(params[:id])

        # render_resource check if user have errors and display them
        user.update(user_params)
        render_resource(user)
      end

      def destroy
        render json: User.find(params[:id]).destroy
      end

      protected

      def user_params
        params.require(:user).permit(:name, :last_name, :password, :email)
      end
    end
  end
end