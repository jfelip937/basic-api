require 'rails_helper'

RSpec.describe 'POST /api/v1/users/sign_in', type: :request do
  let(:user) { FactoryBot.create :user }
  let(:params) do
    {
      user: {
        email: user.email,
        password: user.password
      }
    }
  end

  context 'POST when params are correct' do
    before do
      post new_user_session_url, params: params
    end

    it 'returns 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns JTW token in authorization header' do
      expect(response.headers['Authorization']).to be_present
    end
  end

  context 'when params are incorrect' do
    before { post new_user_session_url }

    it 'returns unathorized status' do
      expect(response.status).to eq 401
    end
  end
end

RSpec.describe 'DELETE /api/v1/users/sign_out', type: :request do
  let(:user) { FactoryBot.create :user }
  let(:params) do
    {
      user: {
        email: user.email,
        password: user.password
      }
    }
  end

  it 'returns 200' do
    delete destroy_user_session_url
    expect(response).to have_http_status(200)
  end

  it 'creates record in the JWT_BlackList table (revoke token)' do
    count = JwtBlacklist.all.count
    delete destroy_user_session_url
    expect(count).to eq(JwtBlacklist.all.count -1)
  end

  context "when signed_out" do
    before do
      post new_user_session_url, params: params
      @token = response.headers['Authorization']
      delete destroy_user_session_url, headers: { 'Authorization' => @token }
    end

    it "authenticated request returns 401" do
      get api_v1_users_url, headers: { 'Authorization' => @token }
      expect(response).to have_http_status(401)
    end
  end
end