require 'rails_helper'

RSpec.describe 'POST /api/v1/users/sign_up', type: :request do
  let(:params) do
    {
      user: {
        name: "Max",
        email: "max@power.com1234",
        password: "asdf1234"
      }
    }
  end

  context 'when user is unauthenticated' do
    before { post user_registration_url, params: params }

    it 'returns 200' do
      expect(response.status).to eq 200
    end

    it 'creates a new user' do
      body_hash = JSON.parse(response.body)
      expect(body_hash['id']).to eq(User.last.id)
    end

    it 'returns the new user' do
      body_hash = JSON.parse(response.body)
      expect(body_hash['email']).to eq(params[:user][:email])
    end
  end

  context 'when user already exists' do
    before do
      FactoryBot.create :user, email: "max@power.com1234"
      post user_registration_url, params: params
    end

    it 'returns bad request status' do
      body_hash = JSON.parse(response.body)
      expect(response.status).to eq 400
      expect(body_hash['errors'].first['title']).to eq('Bad request')
    end

    it 'returns validation errors' do
      body_hash = JSON.parse(response.body)
      expect(body_hash['errors'].first['detail']).to eq({"email"=>["has already been taken"]})
    end
  end

  context 'when some parameter is missing' do

    it 'returns bad request status' do
      post user_registration_url, params: {user: {name: nil, email: "max@power.com1234", password: "asdf1234"}}
      body_hash = JSON.parse(response.body)
      expect(response.status).to eq 400
      expect(body_hash['errors'].first['title']).to eq('Bad request')
    end

    it 'returns name validation errors' do
      post user_registration_url, params: {user: {name: nil, email: "max@power.com1234", password: "asdf1234"}}
      body_hash = JSON.parse(response.body)
      expect(body_hash['errors'].first['detail']).to eq({"name"=>["can't be blank"]})
    end

    it 'returns email validation errors' do
      post user_registration_url, params: {user: {name: "Max", email: nil, password: "asdf1234"}}
      body_hash = JSON.parse(response.body)
      expect(body_hash['errors'].first['detail']).to eq({"email"=>["can't be blank"]})
    end

    it 'returns password validation errors' do
      post user_registration_url, params: {user: {name: "Max", email: "max@power.com1234", password: nil}}
      body_hash = JSON.parse(response.body)
      expect(body_hash['errors'].first['detail']).to eq({"password"=>["can't be blank"]})
    end
  end
end
