module Api
  module V1
    module Users
      class SessionsController < Devise::SessionsController
        respond_to :json

        def create
          super
        end

        private

        def respond_with(resource, _opts = {})
          render json: resource
        end

        def respond_to_on_destroy
          head :ok
        end
      end
    end
  end
end