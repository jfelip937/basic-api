# README

This API is a base for creating diferent APIs. It works with Devise and JSON Web Token for authentication.

This API has the fornt-end basic-front as a default.

**Getting started**

First we'll need to create the .env file or create the following environment variables:
DATABASE_USER(by default root), DATABASE_PASSWORD and DEVISE_JWT_SECRET_KEY. 

DEVISE_JWT_SECRET_KEY should has his own secret key. It can be generated with:

rails secret

Generate the database:

rails db:create

rails db:migrate

rails db:seed

**Starting the server**

rails s -p 3001

Or the port you want it to run.

**TODO**

* Controle Devise routes
* Password update
* Better/Customizable error handling
