class ApplicationController < ActionController::API
  include ActionController::MimeResponds

  # Error handling in all app. Bottom rescues have more priority.
  # General error
  rescue_from StandardError do |e|
    render_json_error :internal_server_error, :internal_server_error
  end

  rescue_from ActiveRecord::RecordNotFound do |e|
    render_json_error :not_found, :product_not_found
  end

  # Method for printing json errors
  def render_json_error(status, error_code = '', detail = nil, options = {})
    status_code = Rack::Utils::SYMBOL_TO_STATUS_CODE[status] if status.is_a? Symbol

    error = {
      title: status.to_s.humanize,
      status: status_code,
      code: error_code
    }.merge(options)

    error[:detail] = detail unless detail.blank?

    render json: { errors: [error] }, status: status_code
  end

  # Method for rendering resources, if the resource has errors (mainly validation error) will display this errors. Used usually by create and update resources methods
  def render_resource(resource)
    if resource.errors.empty?
      render json: resource
    else
      validation_error(resource)
    end
  end

  # Method for printing validation errors from resources, used specially in render_resource
  def validation_error(resource)
    render_json_error :bad_request, '100', resource.errors
  end

  # All responses will be with JSON format
  respond_to :json
end
