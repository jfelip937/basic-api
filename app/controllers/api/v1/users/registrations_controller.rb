module Api
  module V1
    module Users
      class RegistrationsController < Devise::RegistrationsController
        respond_to :json

        def create
          # Allow to add other params than (email, password, password_confirmation) to create registration.
          devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :last_name])

          build_resource(sign_up_params)
          resource.save
          render_resource(resource)
        end
      end
    end
  end
end